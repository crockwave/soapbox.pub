<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Announcing Soapbox FE v1.0 | Soapbox | Social Media Server</title>
    <!-- Meta -->
    <meta name="description" content="Soapbox Frontend is a user interface for the Pleroma social media server. Today we're celebrating its first stable release!">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="msapplication-TileColor" content="#0482d8">
    <meta name="theme-color" content="#0482d8">
    <link rel="manifest" href="/site.webmanifest">
    <!-- Open Graph protocol -->
    <meta property="og:title" content="Announcing Soapbox FE v1.0!">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://soapbox.pub/blog/announcing-soapbox-fe-v1.0/">
    <meta property="og:image" content="https://soapbox.pub/images/logo.svg">
    <meta property="og:description" content="Soapbox Frontend is a user interface for the Pleroma social media server. Today we're celebrating its first stable release!">
    <meta property="og:locale" content="en_US">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/style.css">
    <link rel="stylesheet" href="/styles/fork-awesome.css">
    <!-- Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#0482d8">
  </head>
  <body>
    <div class="topnav">
      <div class="wrap">
        <a class="topnav__logo" href="/">
          <img class="logo" src="/images/logo.svg" alt="Logo">
          Soapbox
        </a>
      </div>
    </div>
    <div class="blog">
      <div class="blog__banner">
        <div class="blog__banner-text">
          <h1>Announcing Soapbox FE v1.0</h1>
          <div class="blog__byline">By Alex Gleason</div>
        </div>
      </div>

      <div class="wrap">
        <img class="screenshot" src="/images/screenshot-inside.png">
      </div>

      <div class="blog__intro">
        <div class="wrap blog__intro-inner">
          <p class="lead">Soapbox Frontend is a user interface for the <a href="https://pleroma.social/" target="_blank">Pleroma</a> social media server. Today we're celebrating its first stable release! 🎉</p>
          <div class="actions">
            <a class="button" href="https://gitlab.com/soapbox-pub/soapbox-fe" target="_blank">
              <i class="fa fa-external-link" aria-hidden="true"></i>
              View code
            </a>
          </div>
        </div>
      </div>

      <div class="wrap">
        <div class="blog__date">Mon, Jun 15, 2020</div>

        <h2>Labor of love</h2>
        <p>After 12 weeks of strenuous labor, we're excited to announce that the Soapbox Frontend is finally stable. Countless hours and late nights were spent pouring love and energy into this release, so that the familiar interface can finally run on Pleroma with minimal gotchas.</p>
        <p>The primary goal of this release was to take the existing interface we had with the legacy Mastodon+Soapbox fork, and stabilize it to work nicely with Pleroma. In future releases we intend to put more effort into design choices and additional features.</p>

        <h2>Why Pleroma?</h2>
        <img class="gallery" src="pleroma-htop.gif">
        <p>The simple answer: Because it's the future.</p>
        <p>Pleroma's <a href="https://pleroma.social/blog/2020/03/08/releasing-pleroma-2-0-0/" target="_blank">2.0 release</a> filled major feature gaps, and set the stage for its role as an innovative fediverse backend. If there were ever a time to jump ship, it's now.</p>
        <p>We are grateful to Mastodon for laying so much groundwork, but Pleroma is pushing things forward: first with EmojiReacts, now with <a href="https://git.pleroma.social/pleroma/pleroma/-/merge_requests/2429" target="_blank">ChatMessages</a>, and with a promise of exciting new features in the pipeline.</p>
        <blockquote>
          <span class="quote">People think you can’t make new activity types. But you can.</span>
          <span class="attribution">&mdash;Lain, creator of Pleroma</span>
        </blockquote>
        <p>Pleroma is worlds more efficient, lowering the barrier of entry for fediverse servers. It's also incredibly flexible, allowing for swappable frontends, which is the feature making Soapbox FE possible. Changing backends was a huge move (more on that later), but will be well worth it in the long term.</p>

        <h2>Server customization</h2>
        <img class="gallery" src="soapbox-branding.gif">
        <p>The big promise of Soapbox is that it supports custom branding. Our philosophy is that fediverse servers should not be cookie-cutter software installations, but should instead appeal to a target demographic. The word "Soapbox" is not displayed in the interface, nor is the Soapbox logo; it's your job to create a name and logo that appeals to your community.</p>
        <p>I believe that we've succeeded in achieving that goal for the v1.0 release, which supports a custom site name, logo, and brand color throughout the interface. The brand color is probably the coolest, since it allows the admin to configure a single hex code to set the color scheme of the entire UI (independently of the light and dark themes).</p>

        <h2>Authentication and settings</h2>
        <img class="gallery" src="edit-profile.png">
        <p>Soapbox FE is based on Mastodon's frontend, which renders some pages in its React webapp, and others with Ruby on Rails. The Ruby on Rails pages could not be carried over, so they had to be rebuilt in React. This is an improvement because it unifies the interface and makes navigation faster.</p>
        <p>The primary areas were authentication (logging in, registering, "I forgot my password", etc.) and user preferences. Users can now log in without reloading the page, and set preferences with immediate results.</p>

        <h2>Emoji reactions</h2>
        <img class="gallery" src="emoji-reacts.gif">
        <p>Finally being on Pleroma, I could not help but implement the EmojiReact feature in this release. For the first release, I aimed to create minimal design changes and to be conservative about the approach.</p>
        <p>While Pleroma offers a lot of flexibility with reactions, Soapbox FE only lets you choose from 6 predefined reactions, and you can only react once to each post. The trade-off of flexibility is simplicity. I believe we're off to a good start with this streamlined UI, and I plan to build more flexibilty onto it in future releases.</p>
        <p>The "👍" reaction gets translated into "Favourite" automatically for compatibility with non-Pleroma servers.</p>

        <h2>Theming</h2>
        <img class="gallery" src="themes.gif">
        <p>The styles have been overhauled using native CSS variables, condensing it to a single stylesheet. This redefines a "theme" to mean a set of CSS variables rather than separate stylesheets for each theme. As a result the bundle is smaller, the SCSS builds faster, and themes can be switched on-the-fly without reloading the page.</p>
        <p>This method was inspired by Pleroma FE's theming system. Thank you HJ for the inspiration.</p>

        <h2>Translations</h2>
        <img class="gallery" src="soapbox-languages.gif">
        <p>Like themes, translations required a total overhaul from the Mastodon way. Previously, Mastodon rendered the user's language in Ruby on Rails, using a complicated build system in Webpack. The new system loads language files in a streamlined way, allowing languages to be switched on-the-fly, and only downloading the language when needed. The old build system was completely removed in favor of these dynamic imports.</p>

        <h2>Code improvements</h2>
        <img class="gallery" src="tests.png">
        <p>Hundreds of lines were refactored, and thousands reformatted. Test coverage increased significantly, now at 10% coverage (compared to Mastodon's 4%). There were some challenges writing tests early-on that have now been overcome, and our intention is that all new code will be tested using TDD.</p>

        <h2>Mastodon to Pleroma Migrator</h2>
        <img class="gallery" src="migrator-photo.jpg">
        <p>Part of this journey was creating a <a href="https://gitlab.com/soapbox-pub/migrator" target="_blank">migration script</a> to migrate existing Mastodon servers into Pleroma. It was an intense challenge that required total focus and perserverance over 12 consecutive days. So far only Gleasonator.com has been migrated this way, but the process is now fairly painless, and all legacy Mastodon+Soapbox servers will be migrated to Pleroma soon enough.</p>

        <h2>Soapbox FE in the wild</h2>
        <p>It has been very exciting to see new communities spring up running Soapbox FE. A huge thank you to everyone who tested it! I'm looking forward to seeing all the colors of the rainbow. 🌈</p>
        <img class="gallery" src="soapbox-homepages.gif">
        <p>Join a Soapbox server today:</p>
        <ul>
          <li><a href="https://gleasonator.com/" target="_blank">Gleasonator</a> - flagship developer instance run by yours truly</li>
          <li><a href="https://social.handholding.io/" target="_blank">Handholding Social</a> - generalistic server with extra features like video streaming</li>
          <li><a href="https://pleroma.nl/" target="_blank">Pleroma 🇳🇱</a> - generalistic Dutch server</li>
          <li><a href="https://social.teci.world/" target="_blank">TECI Social</a> - FOSS-themed server</li>
        </ul>

        <h2>The future</h2>
        <p>Soapbox FE v1.0 is only the beginning. Our vision is to make Pleroma+Soapbox a total replacement for proprietary social media. Some features in the pipeline include:</p>
        <img class="gallery" src="chatmessages.png">
        <ul>
          <li><strong>Chats</strong> - Support for native 'Facebook Messenger'-style chats is already in Pleroma's develop branch, and just needs a UI from us.</li>
          <li><strong>Recurring donations</strong> - A feature originally in the legacy Mastodon+Soapbox, we are rebuilding the integrated recurring donations platform to help servers be sustainable through self-funding by users.</li>
          <li><strong>Groups</strong> - Planned for future Pleroma versions, we hope this feature will enhance discoverability and inspire users to organize.</li>
        </ul>

        <h2>Installing Soapbox FE</h2>
        <p>Installation on an existing Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.</p>
        <h3>0. Install Pleroma</h3>
        <p>If you haven't already follow <a href="https://docs-develop.pleroma.social/backend/installation/debian_based_en/" target="_blank">this guide</a> to install Pleroma.</p>
        <h3>1. Fetch the latest build</h3>
        <div class="code">curl -L https://gitlab.com/soapbox-pub/soapbox-fe/-/jobs/artifacts/v1.0.0/download?job=build-production -o soapbox-fe.zip</div>
        <h3>2. Unpack</h3>
        <div class="code">busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance</div>
        <p>
          <strong>That's it! &#127881; Soapbox FE is installed.</strong>
          The change will take effect immediately, just refresh your browser tab.
          It's not necessary to restart the Pleroma service.
        </p>
        <p>Note that it replaces Pleroma FE. (Running alongside Pleroma FE is planned for a future release). Logged-in users will have to re-authenticate.</p>
        <p>For <strong>removal</strong>, run: <span class="code">rm /opt/pleroma/instance/static/index.html</span></p>
        <h3>3. Customize it</h3>
        <p>You will probably want to customize your site's logo and color scheme. Follow the <a href="https://gitlab.com/soapbox-pub/soapbox-fe/-/blob/master/docs/customization.md" target="_blank">customization docs</a> for instructions on how to do so.</p>

        <h2>Fund the Soapbox project</h2>
        <p>Donations are always optional, but if you'd like to send some money our way you can PayPal it to alex@alexgleason.me</p>

        <h2>Special thanks</h2>
        <p>There were a lot of people who made this release happen. A huge thank you to everyone who got their hands dirty, but also to everyone who expressed enthusiasm, gave moral support, provided feedback, and more. I'm proud of the growing community around this project, and thank you all for being part of this journey.</p>
        <p>In particular, I'd like to thank the following contributors:</p>
        <ul>
          <li><strong>Curtis Rock</strong>, for dedicating countless time, energy, and love into the project. Thank you for the development, documentation, support, and for being an ambassador for the project. Your dedication and overflowing optimism has been a source of inspiration.</li>
          <li><strong>Sean King</strong>, for his bug fixes and relentless enthusiasm.</li>
          <li><strong>Bárbara de Castro Fernandes</strong>, for contributing excellent code to solve difficult problems.</li>
          <li><strong>Lain, and the entire Pleroma dev team</strong>, for building such great software in the first place, and then implementing Pleroma fixes to address key obstacles along the way.</li>
          <li><strong>Sander and Fluffy</strong>, and the users of pleroma.nl and social.handholding.io respectively, for being pioneers of the project and actually <em>running</em> it on real servers! Huge thank you for believing in the project and providing so much useful feedback. (Also, shout-outs to <strong>igel and waltercool</strong> for running it too)</li>
          <li><strong>Isabell Deinschnitzel and matrix07012</strong> for contributing German and Czech translations respectively.</li>
          <li><strong>David Steele, Steven, and other Gleasonator members</strong> who underwent the migration and provided constant support and feedback.</li>
          <li><strong>Mastodon and Gab</strong>, for laying the groundwork for this to happen. Could not have done it without you.</li>
          <li>To everyone else I did not mention, a huge thank you for being part of this journey!</li>
        </ul>
      </div>

      <img class="wave" src="/images/wave.svg">
    </div>

    <div class="blog__footer">
      <div class="wrap">
        <div class="copyheart"><i class="fa fa-heart" aria-hidden="true"></i> 2020. Copying is an act of love. Please copy and share.</div>
      </div>
    </div>
  </body>
</html>
